# MIMIC-III Clinical Database Demo v1.4

This is a copy of the demo database available from:

Johnson, A., Pollard, T., & Mark, R. (2019). MIMIC-III Clinical Database Demo (version 1.4). PhysioNet. https://doi.org/10.13026/C2HM2Q.

[Hands-On Healthcare
Data](https://www.oreilly.com/library/view/hands-on-healthcare-data/9781098112912/)
uses the MIMIC-III demo database so this provides a stable copy in case the
original site is taken down.

## License

Open Data Commons Open Database License v1.0

https://physionet.org/content/mimiciii-demo/view-license/1.4/
